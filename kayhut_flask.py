from flask import Flask, render_template, request, redirect
from code_package import kayhut_simple
import json
app = Flask(__name__)
@app.route('/', methods=["GET", "POST"])
def create_json():
    if request.method == "POST":
        final_dict = {}
        number_check = request.form.get("num")
        if number_check.isdigit():
            number_send = int(number_check)
            final_lst = kayhut_simple.find_prim(number_send)
            final_dict["given_number"] = number_send
            final_dict["prime_numbers_up_to"] = final_lst
            final_json = json.dumps(final_dict, indent=2)
        return final_json
    return render_template("home.html")
if __name__ == '__main__':
    app.run(host="0.0.0.0", debug=True)
