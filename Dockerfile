FROM python:3.9.1
ADD . /python-flask
WORKDIR /python-flask
RUN pip install -r requirements.txt
RUN pip install -r requirements2.txt
COPY . /python-flask

ENTRYPOINT [ "python" ]

CMD [ "kayhut_flask.py" ]